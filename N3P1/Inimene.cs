﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N3P1
{
    public class Inimene
    
    {

        public readonly String Isikukood;               // siin defineerib Inimene, Inimesed ja Isikukood
        public static Dictionary<string, Inimene> Inimesed = new Dictionary<string, Inimene>();


        public Inimene(string isikukood)
        {
            Isikukood = isikukood;
            Inimesed.Add(isikukood, this);
        }

            public static Inimene Create(string isikukood)
        {
            if (Inimesed.ContainsKey(isikukood))
                return Inimesed[isikukood];

            else
                return new Inimene(isikukood);
        }
    }
}
